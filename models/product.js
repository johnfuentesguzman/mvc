const fs = require('fs'); // using file system
const path = require('path'); // creating and using path from operative system (OS

const filepath = path.join(
  path.dirname(process.mainModule.filename),
  'data',
  'products.json'
);

const getProductsFromFile = callBackFunction => { // helper function
  fs.readFile(filepath, (err, fileContent) => {
    if (err) {
      callBackFunction([]);
    } else {
      callBackFunction(JSON.parse(fileContent));
    }
  });
};

module.exports = class Product {
  constructor(t) {
    this.title = t;
  }

  save() {
    getProductsFromFile( (products) => {
      products.push(this);
      fs.writeFile(p, JSON.stringify(products), err => {
        console.log(err);
      });
    });
  }

  static fetchAll(cb) {
    getProductsFromFile(cb);
  }
};
